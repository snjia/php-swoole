<?php

//连接服务器
$client = new \Swoole\Client(SWOOLE_SOCK_TCP);
if(!$client->connect("127.0.0.1", 10005, 0.5)){
    die('连接失败');
}

//发送数据
if(!$client->send('hello swoole')){
    die('发送失败');
}

//接收数据
$data = $client->recv();
if(!$data){
    die('接收数据失败');
}

echo $data;

//关闭连接
$client->close();

