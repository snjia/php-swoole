<?php

/**
 * 原生php
 */

//socket当做成一个网络中的文件
$socket = stream_socket_client('tcp://127.0.0.1:10005', $errno, $errstr, 10);

//发送数据, 也就是写文件
fwrite($socket, "你好, 我是原生php");

//接收数据, 也就是读文件
$buffer = fread($socket, 1024);

//输出
echo $buffer;

//关闭连接
fclose($socket);


