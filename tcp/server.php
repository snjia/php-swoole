<?php

/**
 * $host: 用来指定监听的ip地址  0.0.0.0监听全部地址
 * $port: 监听的端口
 * $mode: SWOOLE_PROCESS多进程模式（默认）  SWOOLE_BASE基本模式
 * $sock_type: 指定Socket的类型  支持TCP、UDP等, 默认SWOOLE_SOCK_TCP
 */
$server = new \Swoole\Server("0.0.0.0", 10005, SWOOLE_PROCESS, SWOOLE_SOCK_TCP);

$server->set([
    //守护进程方式启动
    'daemonize'=>1,
    ///worker_num  设置启动的Worker进程数   CPU核数的1-4倍最合理
    'work_num'=>2
]);

/**
 * 监听连接进入事件
 * $server: swoole_server对象
 * $fd: 连接的文件描述符
 */
$server->on('Connect', function (swoole_server $server, int $fd){
    echo "有新连接: ".$fd.PHP_EOL;
});

/**
 * 监听数据接收事件
 * $reactorId 来自那个reactor线程
 * $data，收到的数据内容
 */
$server->on('Receive', function (swoole_server $server, int $fd, int $reactorId, string $data){
    //向客户端发送数据
    $server->send($fd, "hello client");
    //强制关闭客户端连接
    //$server->close($fd);
    echo "数据: ".$data.PHP_EOL;
});

/**
 * TCP客户端连接关闭后，在worker进程中回调此函数
 */
$server->on('Close', function (swoole_server $server, int $fd, int $reactorId){
    echo $fd."离开了".PHP_EOL;
});

//启动服务
$server->start();

//查看端口是否处于监听状态
//netstat -an | grep 10005

//telnet 测试
//telnet ip 10005