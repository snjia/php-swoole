<?php

$process = new swoole_process(function (swoole_process $pro){
    //在子线程中开启http服务
    $pro->exec("/home/php/php7.4.3/bin/php", [__DIR__.'/../http/server.php']);
}, false);
$pid = $process->start();
echo $pid.PHP_EOL;

//等待子进程退出
swoole_process::wait();


