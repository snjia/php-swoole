<?php

//创建内存表
$table = new swoole_table(1024);

//内存表增加一列
$table->column('id', $table::TYPE_INT, 4);
$table->column('name', $table::TYPE_STRING, 100);
$table->column('age', $table::TYPE_INT, 3);
$table->create();

//设置数据
$table->set('person', ['id'=>1, 'name'=>'张三', 'age'=>11]);

//获取数据
var_dump($table->get('person'));

//对age自增2
$table->incr('person', 'age', 2);
var_dump($table->get('person'));

#自减操作
$table->decr('person', 'age', 1);
var_dump($table->get('person'));

# 删除
$table->del('person');
var_dump($table->get('person'));
